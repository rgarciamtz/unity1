package unity1


import org.junit.*
import grails.test.mixin.*

@TestFor(RegistroController)
@Mock(Registro)
class RegistroControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/registro/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.registroInstanceList.size() == 0
        assert model.registroInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.registroInstance != null
    }

    void testSave() {
        controller.save()

        assert model.registroInstance != null
        assert view == '/registro/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/registro/show/1'
        assert controller.flash.message != null
        assert Registro.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/registro/list'

        populateValidParams(params)
        def registro = new Registro(params)

        assert registro.save() != null

        params.id = registro.id

        def model = controller.show()

        assert model.registroInstance == registro
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/registro/list'

        populateValidParams(params)
        def registro = new Registro(params)

        assert registro.save() != null

        params.id = registro.id

        def model = controller.edit()

        assert model.registroInstance == registro
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/registro/list'

        response.reset()

        populateValidParams(params)
        def registro = new Registro(params)

        assert registro.save() != null

        // test invalid parameters in update
        params.id = registro.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/registro/edit"
        assert model.registroInstance != null

        registro.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/registro/show/$registro.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        registro.clearErrors()

        populateValidParams(params)
        params.id = registro.id
        params.version = -1
        controller.update()

        assert view == "/registro/edit"
        assert model.registroInstance != null
        assert model.registroInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/registro/list'

        response.reset()

        populateValidParams(params)
        def registro = new Registro(params)

        assert registro.save() != null
        assert Registro.count() == 1

        params.id = registro.id

        controller.delete()

        assert Registro.count() == 0
        assert Registro.get(registro.id) == null
        assert response.redirectedUrl == '/registro/list'
    }
}
