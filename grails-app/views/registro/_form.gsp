<%@ page import="unity1.Registro" %>
<g:form action="save" class="register">
    <h1>Inventario de invernadero</h1>
    <fieldset class="row1">

        <p>
        <div class="infobox">

           Notas *<br/>
            <g:textArea name="comenta" value="${registroInstance?.comenta}" rows="5" cols="65"/>
        </div>
        </p>
        <p>
            <div class="infobox">

            Frecuencia de riego *<br/>
            <select name="friego">
                <option value="0">
                <option value="1">Todos los dias
                <option value="2">Dos veces al dia
                <option value="3">Una vez por semana
                </option>
            </select>
            </div>
            <div class="infobox">

            Frecuencia de fertilizacion *<br/>
            <select name="ffertilizacion">
                <option value="0">
                <option value="Una vez por semana">Una vez por semana
                <option value="Una vez al mes">Una vez al mes
                <option value="Cada dos meses">Cada dos meses
            </select>
            </div>
        </p>
        <p>
            <div class="infobox">

            Frecuencia de luz *<br/>
            <select name="fluz">
                <option value="0">
                <option value="Luz solar directa">Luz solar directa
                <option value="Sombra">Sombra
            </select>
            </div>
            <div class="infobox">

            Frecuencia de temperatura *<br/>
            <select name="ftemperatura">
                <option value="0">
                <option value="Templado">Templado
                <option value="Cálido">Cálido
                <option value="Caluroso">Caluroso
            </select>
            </div>
        </p>



    </fieldset>
    <fieldset class="row1">


        <p>

            <div class="infobox">
            Fecha de poda *<br/>
            <g:datePicker name="dpoda" value="${new Date()}" precision="day" years="${1950..2020}"/>
            </div>
            <div class="infobox">
            Fecha de riego *<br/>
            <g:datePicker name="driego" value="${new Date()}" precision="day" years="${1950..2020}"/>
            </div>

        </p>
        <p>

        <div class="infobox">
            Fecha de compra *<br/>
            <g:datePicker name="dcompra" value="${new Date()}" precision="day" years="${1950..2020}"/>
        </div>
        <div class="infobox">
            Fecha de plantacion *<br/>
            <g:datePicker name="dplantacion" value="${new Date()}" precision="day" years="${1950..2020}"/>
        </div>

        </p>
    <p>

    <div>
    <input type="file" name="fileup" id="redemptionFile" class="btn btn-info"/>
    <small></small>
    </div>

    </p>
    <p>
    <div class="infobox">
        Nombre vulgar *<br/>
        <input type="text" name="nvulgar"/>
    </div>
    </p>
    <p>
    <div class="infobox">
        Genero *<br/>
        <input type="text" name="genero" />
    </div>
    <div class="infobox">
        Especie *<br/>
        <input type="text" name="especie" />
    </div>
    </p>
    <p>




</fieldset>
    <div><button class="button" type="submit">Enviar</button></div>

</g:form>



