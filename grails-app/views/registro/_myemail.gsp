<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Jaime Quintanilla | Gracias</title>

    <style type="text/css">
    h2{
        font-size: 22px;
        font-weight: bold;
        color: #b4262c;
        line-height: 150%;
        height:60px;

    }

    a{
        font-size: 1em;
        font-weight: bold;
        color: #177232;
        line-height: 150%;
        height:60px;

    }

    </style>

</head>

<body style="margin:0; font-family:'Helvetica Neue', Helvetica, Arial, sans-serif; background-color:#ffffff;">

<table width="700" border="0" cellspacing="0" cellpadding="0" margin="0" align="center" bgcolor="white">
    <tr>
        <td bgcolor="#E4E6EB" colspan="2"><img src="http://quaxar.com/PVEM/Header.jpg" width="700" height="295" alt=""/></td>
    </tr>
    <tr>
        <td bgcolor="#E4E6EB" height="90" colspan="2" style="font-size:2em; padding:0.2em 0.6em; text-align:center; font-weight:bold; color:#177232; line-height:0.9em;">¡Muchas Gracias por Registrarte!
        </td>
    </tr>
    <tr>
        <td  bgcolor="#E4E6EB"colspan="2" style="font-size:1em; padding:0 2.6em; padding-bottom:2.2em; text-align:justify;">Ya eres parte de esta red ciudadana de apoyo a <strong>Jaime Quintanilla</strong>, juntos podremos hacer la diferencia. <br><br>

            Es importante que, en la medida de lo posible, invites a tus amigos, familiares y conocidos a formar parte de esta red a fin de que logremos ser los ciudadanos suficientes para marcar un cambio tan necesario en Coatzacoalcos.<br><br>

            Si no sabes dónde invitar a tus conocidos a formar parte de 10ConJaime, puedes hacerlo <strong><a href="#" target="_blank">aquí</a></strong>.<br><br>

            Para conocer más sobre las propuestas de Jaime Quintanilla y estar al tanto de lo que va sucediendo con la campaña, te invitamos a conocer los sitios que tenemos disponibles para este fin.<br><br>


        </td> </tr>

    <tr>
        <td colspan="2" style="padding-top:0em"><img src="http://quaxar.com/PVEM/footer.png" alt="" width="700" height="90" usemap="#Map"/></td>
    </tr>
</table>


<map name="Map"><area shape="rect" coords="372,17,410,50" href="https://www.instagram.com/JaimeCoatza/" target="_blank"><area shape="rect" coords="328,18,366,51" href="https://twitter.com/jaimecoatza" target="_blank">
    <area shape="rect" coords="241,54,452,88" href="http://www.jaimequintanilla.com.mx/" target="_blank" alt="site">
    <area shape="rect" coords="280,18,318,51" href="https://www.facebook.com/JaimeQuintanillaCoatza/" target="_blank">
</map>

</body>
</html>