package unity1

import org.springframework.dao.DataIntegrityViolationException

class RegistroController {
    def inventoryService
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
       // redirect(action: "list", params: params)

        redirect(action:"create", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [registroInstanceList: Registro.list(params), registroInstanceTotal: Registro.count()]
    }

    def create() {
        def obtDat = inventoryService.getCountlines()
        //render(template:"form", model: [obtDat: obtDat])
        [registroInstance: new Registro(params),obtDat:obtDat]

    }

    def obtDatos() {


    }

    def save() {
        sendMail {
            to "isc.rgm@gmail.com"
            subject "The issue you watch has been updated"
            html g.render(template:"myemail")
        }
        def registroInstance = new Registro(params)


        switch (registroInstance.friego){
           case "1":
               registroInstance.friego = 'Todos los dias'
               break
           case "2":
               registroInstance.friego = 'Dos veces al dia'
               break
           case "3":
               registroInstance.friego = 'Una vez por semana'
               break
        }


        println (params)
        if (!registroInstance.save(flush: true)) {
            render(view: "create", model: [registroInstance: registroInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'registro.label', default: 'Registro'), registroInstance.id])
        //redirect(action: "show", id: registroInstance.id)
        render(view: "gracias")
    }

    def show(Long id) {
        def registroInstance = Registro.get(id)
        if (!registroInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'registro.label', default: 'Registro'), id])
            redirect(action: "list")
            return
        }

        [registroInstance: registroInstance]
    }

    def edit(Long id) {
        def registroInstance = Registro.get(id)
        if (!registroInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'registro.label', default: 'Registro'), id])
            redirect(action: "list")
            return
        }

        [registroInstance: registroInstance]
    }

    def update(Long id, Long version) {
        def registroInstance = Registro.get(id)
        if (!registroInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'registro.label', default: 'Registro'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (registroInstance.version > version) {
                registroInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'registro.label', default: 'Registro')] as Object[],
                          "Another user has updated this Registro while you were editing")
                render(view: "edit", model: [registroInstance: registroInstance])
                return
            }
        }

        registroInstance.properties = params

        if (!registroInstance.save(flush: true)) {
            render(view: "edit", model: [registroInstance: registroInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'registro.label', default: 'Registro'), registroInstance.id])
        redirect(action: "show", id: registroInstance.id)
    }

    def delete(Long id) {
        def registroInstance = Registro.get(id)
        if (!registroInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'registro.label', default: 'Registro'), id])
            redirect(action: "list")
            return
        }

        try {
            registroInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'registro.label', default: 'Registro'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'registro.label', default: 'Registro'), id])
            redirect(action: "show", id: id)
        }
    }
}
