package unity1

class Registro {
    String comenta
    String friego
    String ffertilizacion
    String fluz
    String ftemperatura
    Date dpoda
    Date driego
    Date dcompra
    Date dplantacion
    String fileup
    String nvulgar
    String genero
    String especie

    static constraints = {
        comenta          blank: false, nullable: false
        friego           blank: false, nullable: false
        ffertilizacion   blank: false, nullable: false
        fluz             blank: false, nullable: false
        ftemperatura     blank: false, nullable: false
        dpoda            blank: false, nullable: false
        driego           blank: false, nullable: false
        dcompra          blank: false, nullable: false
        dplantacion      blank: false, nullable: false
        fileup           blank: false, nullable: false
        nvulgar          blank: false, nullable: false
        genero           blank: false, nullable: false
        especie          blank: false, nullable: false
    }
}
