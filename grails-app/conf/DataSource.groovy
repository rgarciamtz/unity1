dataSource {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    username = "sa"
    password = ""
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
    development {
        dataSource {
            pooled = true
            dbCreate = "update"
            url = "jdbc:mysql://127.0.0.1:3306/unity?connectTimeout=1200000&socketTimeout=1200000&autoReconnect=true"
            driverClassName = "com.mysql.jdbc.Driver"
            dialect = org.hibernate.dialect.MySQL5InnoDBDialect
            username = "root"
            password = "00562793"
            properties {
                maxActive = 50
                maxIdle = 25
                minIdle = 5
                initialSize = 5
                minEvictableIdleTimeMillis = 60000
                timeBetweenEvictionRunsMillis = 60000
                maxWait = 10000
                validationQuery = "SELECT 1"
                testOnBorrow=true
            }
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            url = "jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
        }
    }
    production {
        dataSource {
            dbCreate = "update"
            url = "jdbc:mysql://localhost:3306/unity"
            driverClassName = "com.mysql.jdbc.Driver"
            username = "root"
            password = "00562793"
            properties{
                maxActive=100
                maxIdle=50
                maxWait=10000
                testOnBorrow=false
                testOnReturn=false
                testWhileIdle=true
                validationQuery="Select 1"
                timeBetweenEvictionRunsMillis=1800000
            }
        }
    }
}



